﻿using System;

namespace BMI_Calc
{
    class Program
    {
        static void Main(string[] args)
        {
            float height = InputHeight();
            float weight = InputWeight();
            CalculateBmi(height, weight); ;
        }
        public static void CalculateBmi(float height, float weight)
        {
           
            float bmi = weight / (height * height);

            if (bmi < 18.5) { Console.WriteLine("Underweight!"); }
            else if (bmi >= 18.5 && bmi < 25) { Console.WriteLine("Normal weight"); }
            else if (bmi >= 25 && bmi < 30) { Console.WriteLine("Overweight"); }
            else if (bmi > 30) { Console.WriteLine("Obese"); }
        }
        public static float InputHeight()
        {
            Console.WriteLine("Enter height(m)");
            float height = float.Parse(Console.ReadLine());
            return height;
            
                        
        }
        public static float InputWeight()
        {
            Console.WriteLine("Enter Weight(kg)");
            float weight = float.Parse(Console.ReadLine());
            return weight;
        }
    }
    
}
